(load-library "haskell-mode-autoloads")
(load-file "test/run.el")

;; stack will fail if $HOME is not a writable directory, and the
;; autopkgtest specification does not guarantee that it is.  So tell
;; stack to work somewhere else
(setenv "STACK_ROOT" (or (getenv "AUTOPKGTEST_TMP")
                         (getenv "ADT_TMP")))

(flycheck-run-tests-main)
