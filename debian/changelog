flycheck (32-1) unstable; urgency=medium

  * new upstream release
  * update copyright years
  * remove Built-Using field as suggested by lintian
  * Standards-Version: 4.6.0 (stop using Built-Using field)

 -- Denis Danilov <danilovdenis@yandex.ru>  Sat, 02 Apr 2022 14:24:08 +0200

flycheck (32~git.20200527.9c435db3-2) unstable; urgency=medium

  * skip truncated-stdin-* tests
    - the tests are flaky on amd64 and ppc64el

 -- Denis Danilov <danilovdenis@yandex.ru>  Wed, 25 Nov 2020 22:37:27 +0100

flycheck (32~git.20200527.9c435db3-1) unstable; urgency=medium

  [ David Krauser ]
  * Update maintainer email address

  [ Denis Danilov ]
  * set Rules-Requires-Root: no
  * set debhelper-compat level to 13
  * Standards-Version: 4.5.0 (no changes required)
  * adopt DEP-14 recommendations
  * new upstream git snapshot from flycheck-32 branch 
  * rebase patches
  * update copyright years

 -- Denis Danilov <danilovdenis@yandex.ru>  Sun, 08 Nov 2020 19:20:03 +0100

flycheck (31+git.20190913.0006a592-1) unstable; urgency=medium

  * new upstream git snapshot
  * rebase patches
    - drop unused Don-t-fail-if-stack-path-project-root-fails.patch to fix
      lintian warning
  * cleanup salsa pipeline config
  * update Standards-Version to 4.4.0 (no changes needed)
  * optimize d/control
    - Build-Depends: depend on python3 and elpa-haskell-mode
    - Enhances: drop emacs24
  * python3 in test
    - switch to py3 because py2 is going to be removed in bullseye,
      see https://wiki.debian.org/Python/2Removal
  * fix renamed lintian tag
  * skip flaky tests with ert-skip
    - refactor patches disable-sqlint-test.patch,
      disable-rpmlint-test.patch and disable-flaky-tests.patch
      into skip-sqlint-test.patch, skip-rpmlint-test.patch and
      skip-flaky-tests.patch correspondingly
    - it will make it transparent in the test report how many tests
      are actually skipped

 -- Denis Danilov <danilovdenis@yandex.ru>  Sat, 28 Sep 2019 19:52:23 +0200

flycheck (31-4) UNRELEASED; urgency=medium

  * update salsa pipeline
  * rebase and cleanup patches
  * skip flaky tests with ert-skip

 -- Denis Danilov <danilovdenis@yandex.ru>  Thu, 19 Sep 2019 07:59:54 +0200

flycheck (31-3) unstable; urgency=medium

  * Team upload.

  [ Denis Danilov ]
  * disable intersphinx and info extensions (Closes: #929232)

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 21 May 2019 13:23:38 -0700

flycheck (31-2) unstable; urgency=medium

  * enable GitLab CI at salsa.d.o
  * adopt package: remove Sean from uploaders and add myself (closes: #904240)
  * update Standards-Version to 4.3.0 (no changes required)
  * Build-Depend on debhelper-compat 12
    - drop obsolete debian/compat
    - drop --parallel from debian/rules (it is enabled by default)
  * update project homepage
  * set Vcs-* to salsa.d.o
  * cleanup Build-Depends field
    - sort the list of package
    - drop redundant pyton3-docutils and python3-requests because
      python3-sphinx itself depends on them directly

 -- Denis Danilov <danilovdenis@yandex.ru>  Thu, 03 Jan 2019 20:20:26 +0100

flycheck (31-1) unstable; urgency=medium

  * New upstream release (Closes: #877411).
    - Updates to d/copyright.
  * Update dependency dash-el => elpa-dash (Closes: #865910).
    Thanks to Mykola Nikishov for reporting the problem.
    - Bump dh-elpa build dependency to (>= 1.7).
      Ensures that elpa-dash is included in ${elpa:Depends}.
  * Bump debhelper build-dep to match compat.
    - Drop Lintian override package-needs-versioned-debhelper-build-depends.
  * Drop fix-flake8-expected-values.patch
    Obsoleted by upstream changes.
  * Refresh patches.
  * Bump standards version to 4.1.1 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 07 Oct 2017 08:42:58 -0700

flycheck (30-3) unstable; urgency=low

  * Add disable-flaky-tests.patch
    At present, it is not feasible to keep the expected values of these
    tests up-to-date.  See patch header.
  * Stop skipping the ERT tests during the package build.
    The particular tests that necessitated skipping the ERT test suite
    altogether are now disabled by disable-flaky-tests.patch.
    - Remove ERT skipping code & comments from d/ert-helper.el
    - Add build-deps on haskell-mode, python-minimal
    - Set Testsuite: autopkgtest-pkg-elpa
    - Drop d/tests/control
      This file was needed to list the extensive dependencies of the tests
      disabled by disable-flaky-tests.patch, plus the general dependencies
      of the ERT test suite.  The latter are now folded into d/control.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 18 Jan 2017 21:45:22 -0700

flycheck (30-2) unstable; urgency=medium

  * Drop gnupg1 build dependency (Closes: #848868).
    This was added so the "recognizes an encrypted buffer" test could pass
    under emacs24.  However, upstream has since disabled that test for
    emacs24 and below.
  * Drop use-gpg1.patch
    Similarly obsoleted by upstream change.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 22 Dec 2016 08:24:57 +0000

flycheck (30-1) unstable; urgency=medium

  * New upstream release.
  * Quilt patch changes:
    - Drop fix-autoload-tests.patch
      Merged upstream.
    - Drop remove-references-to-legacy-manual.patch
      Merged upstream.
    - Drop remove-todolist.patch
      Merged upstream.
    - Drop patch-test-runner.patch
      Obsolete: the logic we were patching has moved to the Makefile,
      which we bypass anyway.
    - Update use-gpg1.patch to apply to test-gpg.el, not test-util.el.
    - Add Don-t-fail-if-stack-path-project-root-fails.patch
    - Some other patches refreshed.
  * Set flycheck-doc Built-Using.
    - Tighten python3-sphinx build-dep for ${sphinxdoc:Built-Using}.
  * Drop d/source/local-options for dgit.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 25 Oct 2016 07:56:41 -0700

flycheck (29-2) unstable; urgency=medium

  * Drop patches concerning gpg ERT tests:
    - clear-GPG-agent-env-vars.patch
    - set-epg-gpg-home-directory.patch
    gpg tests have been moved from the ERT suite to the Buttercup suite.
  * Set STACK_ROOT=$AUTOPKGTEST_TMP for ERT tests.
    Attempt to avoid stack tests failing on ci.debian.net.
  * Refresh patch-test-runner.patch

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 02 Oct 2016 19:50:27 -0700

flycheck (29-1) unstable; urgency=medium

  * New upstream release.
    - New build-dep on elpa-f
  * Build new offline docs in new flycheck-doc binary package.
    - flycheck suggests flycheck-doc
    - Build-depend on python3-{sphinx,requests,docutils}
    - Declare build conflict with python-sphinx
    - Call dh --with sphinxdoc
    - Add d/clean, d/flycheck-doc.doc-base, d/flycheck-doc.docs
  * Add elpa-geiser dependency to d/tests/control.
    Enable some more tests.
  * Test for ADT_TMP as well as AUTOPKGTEST_TMP.
    For compatibility with ci.debian.net
  * Drop obsolete paragraph from d/copyright.
    Upstream deleted test/specs/test-code-style.el
  * Update d/copyright information for doc/ dir.
  * Remove references to legacy manual that we don't install:
    - Extend patch-README-for-Debian.patch
    - Add remove-references-to-legacy-manual.patch
  * Add remove-todolist.patch
  * Drop patches merged upstream:
    - Update-expected-values-for-cppcheck-1.74.patch
    - pass-epg-gpg-home-directory-to-gpg-invocation.patch
    - set-C-locale-and-patch-C-and-fortran-tests.patch
    - update-expected-values-for-gcc-6.patch
    - update-expected-values-for-rustc-version-1.9.0.patch
    - update-expected-values-for-rustc-1.10.patch
    - update-perl-expected-values.patch
  * Drop obsolete patch disable-code-style-spec-test.patch.
  * Refresh patches.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 01 Sep 2016 17:05:33 -0700

flycheck (28-6) unstable; urgency=medium

  * Skip ERT tests during package build.
    See comments in debian/ert-helper.el.
  * Add d/tests/control & move ERT build dependencies there from d/control.
  * Add Testsuite: field to d/control.
  * Delete obsolete README.source.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 23 Aug 2016 17:59:42 -0700

flycheck (28-5) unstable; urgency=medium

  * Strip numbers from patch filenames.
  * Add some missing Forwarded: headers to patches.
  * Move Forwarded: headers so they won't be stripped by gbp.
    This method from Dmitry Bogatov's packaging work.
    (The patch headers are not DEP-3 compliant anyway: see #785274.)
  * Add use-gpg1.patch  (Closes: #818142)
    - Switch build dependency gnupg -> gnupg1.
      See header of use-gpg1.patch for details.
  * Drop update-shellcheck-expected-values.patch  (Closes: #818142)
    Debian version now the same with upstream's testing platform.
  * Add update-perl-expected-values.patch.  (Closes: #818142)
  * Add disable-rpmlint-test.patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 22 Aug 2016 21:19:14 -0700

flycheck (28-4) unstable; urgency=medium

  * Add 0018-update-expected-values-for-gcc-6.patch.
  * Move dh_elpa_test config to d/elpa-test.
    - Tighten build-dep on dh-elpa.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 04 Aug 2016 12:06:08 -0700

flycheck (28-3) unstable; urgency=medium

  * Add 0017-update-expected-values-for-rustc-1.10.patch

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 03 Aug 2016 17:26:38 -0700

flycheck (28-2) unstable; urgency=medium

  * Set DH_ELPA_TEST_AUTOPKGTEST_KEEP in d/rules.
  * Add 0014-Update-expected-values-for-cppcheck-1.74.patch.
    Fixes build in current sid.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 07 Jul 2016 13:24:32 +0900

flycheck (28-1) unstable; urgency=medium

  * Package new upstream release.
  * Drop 0012-update-rustc-expected-values.patch.
    Obsolete with latest rustc.
  * Drop 0014-Fix-cppcheck-test.patch.
    Included in this upstream release.
  * Refresh patches.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 10 Jun 2016 16:13:54 +0900

flycheck (27-1) unstable; urgency=medium

  * Package new upstream release.
  * Run test suite with dh_elpa_test.
    - Bump debhelper compat level to 10.
    - Override Lintian tag due to using experimental compat level:
      package-needs-versioned-debhelper-build-depends.
  * Build dependency changes:
    - New build-dep for testsuite: cppcheck.
    - New build dependency on elpa-buttercup for dh_elpa_test.
    - Build dependency on dh-elpa tightened for dh_elpa_test.
    - Tighten build dependencies on hlint and rustc to ensure test suite passes.
  * Quilt patch changes:
    - Drop 0004-update-pylint-expected-values.patch.
      Merged upstream.
    - Drop 0010-skip-haskell-stack-tests-when-homedir-nonexistent.patch.
      Merged upstream.
    - Drop 0011-hack-Makefile-to-silence-cmd-not-found-errors.patch.
      No longer required.
    - Add 0010-disable-MELPA-tests.patch.
    - Add 0011-disable-code-style-spec-test.patch.
    - Add 0013-patch-test-runner.patch.
    - Add 0014-Fix-cppcheck-test.patch.
    - Add 0015-pass-epg-gpg-home-directory-to-gpg-invocation.patch.
    - Add 0016-update-expected-values-for-rustc-version-1.9.0.patch.
    - Refresh remaining patches.
  * Documentation changes:
    - Remove d/copyright entry for obsoleted file test/flycheck-checkdoc.el.
    - Add d/copyright entry for new file test/test-code-style.el.
    - Drop debian/info since upstream's info manual no longer exists.
    - Don't install CREDITS.md since upstream's credits file no longer exists.
    - Merge and install two upstream changelogs.
  * Override dh_auto_build to avoid Cask.
  * Remove comments from d/control due to #823708.
  * Create README.source with information from comments.
  * Run wrap-and-sort -abst.
  * Bump standards version to 3.9.8 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 06 Jun 2016 12:59:44 +0900

flycheck (0.25.1-1) unstable; urgency=medium

  * Initial release. (Closes: #796652)

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 10 Mar 2016 20:56:15 -0700
